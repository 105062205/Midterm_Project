# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Shopping Website]
* Key functions (add/delete)
    1. [Home Page] index.html
    2. [Product page] index.html section #portfolio
    3. [Shopping pipeline] cart.html
    4. [Order Detail] index.html
    5. [Signup/Login Page] auth.html
* Other functions (add/delete)
    1. [Virtual company introduction] index.html section #services
    2. [Contact] index.html section #contact
    3. [Check out form] check.html
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|Y| signin logout notification
|Use CSS Animation|2.5%|Y| cart.html background check.html background homepage scroll
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
這是一個賣茶葉的購物網站，在首頁有虛擬公司的簡介，產品與標價（點擊以後會有大圖詳細介紹產品），以及聯絡方式。在開始購物之前，會先要求購物者登入;點擊shop now後可以添加刪除、調整欲購買產品的數量，確定後點擊checkout會進入結帳的畫面，填寫基本資料後，點擊confirm info後點擊checkout就完成購買的流程，畫面會跳轉回首頁，此時點開order detail 可以看到自己的訂單明細
## Security Report (Optional)
