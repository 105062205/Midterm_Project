var breakfastQ = 1,
    earlQ = 1,
    greenQ = 1,
    chaiQ = 1,
    mintQ = 1,
    fruitQ = 1;
var breakfastName = "breakfast",
    earlName = "earl",
    greenName = "green",
    chaiName = "chai",
    mintName = "mint",
    fruitName = "fruit";
var breakfastP = 185,
    earlP = 185,
    greenP = 225,
    chaiP = 220,
    mintP = 250,
    fruitP = 300;
    // window.onload=function()
function init(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            var userId = user.uid;
            console.log(userId);
            var break_btn = document.getElementById('breakfast-btn');
            var earl_btn = document.getElementById('earl-btn');
            var green_btn = document.getElementById('green-btn');
            var chai_btn = document.getElementById('chai-btn');
            var mint_btn = document.getElementById('mint-btn');
            var fruit_btn = document.getElementById('fruit-btn');

            var dlt_breakfast = document.getElementById('dlt-breakfast');
            var dlt_earl = document.getElementById('dlt-earl');
            var dlt_green = document.getElementById('dlt-green');
            var dlt_chai = document.getElementById('dlt-chai');
            var dlt_mint = document.getElementById('dlt-mint');
            var dlt_fruit = document.getElementById('dlt-fruit');

            
            
            
            dlt_breakfast.addEventListener('click', e => {
                var breakval = document.getElementById('breakfastq');
                breakval.value = 0;
                var newitem = firebase.database().ref(userId + '/cart/'+'item1/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
            });
            dlt_earl.addEventListener('click', e => {
                var earlval = document.getElementById('earlq');
                earlval.value = 0;
                var newitem = firebase.database().ref(userId +'/cart/'+'item2/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
            });
            dlt_green.addEventListener('click', e => {
                var greenval = document.getElementById('greenq');
                greenval.value = 0;
                var newitem = firebase.database().ref(userId +'/cart/'+'item3/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                
            });
            dlt_chai.addEventListener('click', e => {
                var chaival = document.getElementById('chaiq');
                chaival.value = 0;
                var newitem = firebase.database().ref(userId + '/cart/'+'item4/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
            });
            dlt_mint.addEventListener('click', e => {
                var mintval = document.getElementById('mintq');
                mintval.value = 0;
                var newitem = firebase.database().ref(userId + '/cart/'+'item5/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
            });
            dlt_fruit.addEventListener('click', e => {
                var fruitval = document.getElementById('fruitq');
                fruitval.value = 0;
                var newitem = firebase.database().ref(userId + '/cart/'+'item6/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
            });

            break_btn.addEventListener('click', e => {
                if(breakfastQ!=0){
                    var newitem = firebase.database().ref(userId + '/cart/'+'item1/');
                    newitem.set({
                    product_name: breakfastName,
                    product_price: breakfastP,
                    product_quantity: breakfastQ
                    })
                }else{
                    var newitem = firebase.database().ref(userId +'/cart/'+'item1/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                }
                
            });
            earl_btn.addEventListener('click', e => {
                if(earlQ!=0){
                    var newitem = firebase.database().ref(userId +'/cart/'+'item2/');
                    newitem.set({
                        product_name: earlName,
                        product_price: earlP,
                        product_quantity: earlQ
                    })
                }else{
                    var newitem = firebase.database().ref(userId +'/cart/'+'item2/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                }
            });
            green_btn.addEventListener('click', e => {
                if(greenQ!=0){
                    var newitem = firebase.database().ref(userId +'/cart/'+'item3/');
                    newitem.set({
                        product_name: greenName,
                        product_price: greenP,
                        product_quantity: greenQ
                    })
                }else {
                    var newitem = firebase.database().ref(userId +'/cart/'+'item3/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                }
            });
            chai_btn.addEventListener('click', e => {
                if(chaiQ!=0){
                    var newitem = firebase.database().ref(userId +'/cart/'+'item4/');
                    newitem.set({
                        product_name: chaiName,
                        product_price: chaiP,
                        product_quantity: chaiQ
                    })
                }else {
                    var newitem = firebase.database().ref(userId +'/cart/'+'item4/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                }
            });
            mint_btn.addEventListener('click', e => {
                if(mintQ!=0){
                    var newitem = firebase.database().ref(userId +'/cart/'+'item5/');
                    newitem.set({
                        product_name: mintName,
                        product_price: mintP,
                        product_quantity: mintQ
                    })
                }else {
                    var newitem = firebase.database().ref(userId +'/cart/'+'item5/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                }
            });
            fruit_btn.addEventListener('click', e => {
                if(fruitQ!=0){
                    var newitem = firebase.database().ref(userId +'/cart/'+'item6/');
                    newitem.set({
                        product_name: fruitName,
                        product_price: fruitP,
                        product_quantity: fruitQ
                    })
                }else {
                    var newitem = firebase.database().ref(userId +'/cart/'+'item6/');
                    newitem.set({
                        product_name: null,
                        product_price: null,
                        product_quantity: null
                    })
                }
            });
        } else {
         console.log("No user is signed in.");
        }
      });

    
}

function incr(obj){
    var name = obj.getAttribute('data-name');
    switch(name){
        case "breakfast":
            var item = document.getElementById('breakfastq');
            item.value ++;
            breakfastQ = item.value;
            break;
        case "earl":
            var item = document.getElementById('earlq');
            item.value ++;
            earlQ = item.value;
            break;
        case "green":
            var item = document.getElementById('greenq');
            item.value ++;
            greenQ = item.value;
            break;
        case "chai":
            var item = document.getElementById('chaiq');
            item.value ++;
            chaiQ = item.value;
            break;
        case "mint":
            var item = document.getElementById('mintq');
            item.value ++;
            mintQ = item.value;
            break;
        case "fruit":
            var item = document.getElementById('fruitq');
            item.value ++;
            fruitQ = item.value;
            break;
    }
}
function decr(obj){
    var name = obj.getAttribute('data-name');
    switch(name){
        case "breakfast":
            var item = document.getElementById('breakfastq');
            if(item.value==0) break;
            item.value --;
            breakfastQ = item.value;
            break;
        case "earl":
            var item = document.getElementById('earlq');
            if(item.value==0) break;
            item.value --;
            earlQ = item.value;
            break;
        case "green":
            var item = document.getElementById('greenq');
            if(item.value==0) break;
            item.value --;
            greenQ = item.value;
            break;
        case "chai":
            var item = document.getElementById('chaiq');
            if(item.value==0) break;
            item.value --;
            chaiQ = item.value;
            break;
        case "mint":
            var item = document.getElementById('mintq');
            if(item.value==0) break;
            item.value --;
            mintQ = item.value;
            break;
        case "fruit":
            var item = document.getElementById('fruitq');
            if(item.value==0) break;
            item.value --;
            fruitQ = item.value;
            break;
    }
}

function amFunction(x) {
    x.classList.toggle("change");
    setTimeout(function(){
        window.location = "index.html";
    }, 1000);
}