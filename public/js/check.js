var item1=0,
    item2=0,
    item3=0,
    item4=0,
    item5=0,
    item6=0;
    

function init(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            var userId = user.uid;
            console.log(userId);
            // CART
            var breakRef = firebase.database().ref(userId+'/cart/').child('item1');
            breakRef.once('value',setBreak);

            var earlRef = firebase.database().ref(userId+'/cart/').child('item2');
            earlRef.once('value',setEarl);

            var greenRef = firebase.database().ref(userId+'/cart/').child('item3');
            greenRef.once('value',setGreen);
            
            var chaiRef = firebase.database().ref(userId+'/cart/').child('item4');
            chaiRef.once('value',setChai);

            var mintRef = firebase.database().ref(userId+'/cart/').child('item5');
            mintRef.once('value',setMint);

            var fruitRef = firebase.database().ref(userId+'/cart/').child('item6');
            fruitRef.once('value',setFruit);

            var totalRef = firebase.database().ref(userId+'/cart/');
            totalRef.once('value', setTotal);

            
            //CHECKOUT BUTTON
            var confirm_btn = document.getElementById('confirmbtn');
            var checkout_btn = document.getElementById('checkoutbtn');
            // PAYMENT
            var c_name = document.getElementById('cname');
            var c_num = document.getElementById('ccnum');
            var exp_month = document.getElementById('expmonth');
            var exp_year = document.getElementById('expyear');
            var c_vv = document.getElementById('cvv');
            // BILLING ADDRESS
            var f_name = document.getElementById('fname');
            var e_mail = document.getElementById('email');
            var a_dr = document.getElementById('adr');
            var c_ity = document.getElementById('city');
            var p_hone = document.getElementById('state');
            var z_ip = document.getElementById('zip');
            var totalp = document.getElementById('totalpay');
            confirm_btn.addEventListener('click', e => {
                var newinfo = firebase.database().ref('users/'+ userId+'/info');
                newinfo.set({
                    cardname: c_name.value,
                    cardnumber: c_num.value,
                    expiremonth: exp_month.value,
                    expireyear: exp_year.value,
                    cvv: c_vv.value,
                    fullname: f_name.value,
                    email: e_mail.value,
                    address: a_dr.value,
                    city: c_ity.value,
                    phone: p_hone.value,
                    zip: z_ip.value
                })
                var newbill = firebase.database().ref('users/'+ userId+'/order');
                newbill.set({
                    payment: totalp.value,
                    breakfast: item1,
                    earlgrey: item2,
                    green: item3,
                    chai: item4,
                    mint: item5,
                    fruit: item6
                })
            });
            checkout_btn.addEventListener('click', e => {
                var removecart = firebase.database().ref(userId);
                removecart.remove();
                window.location = "index.html";
            });
          // User is signed in.
        } else {
          // No user is signed in.
          console.log("No user is signed in");
          window.location = "auth.html";
        }
      });
}
function setTotal(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var keys = Object.keys(data);
        var i = keys[0];
        var temp=0;
        var count=0;

        for(var i in keys){
            var k = keys[i];
            temp += data[k].product_price * data[k].product_quantity;
            if(data[k].product_quantity) count+= Number(data[k].product_quantity);
        }
        var total = document.getElementById('totalpay');
        total.value = temp;
        total.innerHTML = 'NT ' + temp;
        var amount = document.getElementById('amount');
        amount.value = count;
        amount.innerHTML = count;
    }
}
function setBreak(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var pay = data.product_price * data.product_quantity;
        item1 = data.product_quantity;
        var subtotal = document.getElementById('breakpay');
        var count = document.getElementById('breakc');
        subtotal.innerHTML = 'NT ' + pay;
        count.innerHTML = ' ' + '*' + item1;
    }else{
        var count = document.getElementById('breakc');
        count.innerHTML = ' ';
    }
}
function setEarl(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var pay = data.product_price * data.product_quantity;
        item2 = data.product_quantity;
        var subtotal = document.getElementById('earlpay');
        var count = document.getElementById('earlc');
        subtotal.innerHTML = 'NT ' + pay;
        count.innerHTML = ' ' + '*' + item2;
    }else{
        var count = document.getElementById('earlc');
        count.innerHTML = ' ';
    }
}
function setGreen(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var pay = data.product_price * data.product_quantity;
        item3 = data.product_quantity;
        var subtotal = document.getElementById('greenpay');
        var count = document.getElementById('greenc');
        subtotal.innerHTML = 'NT ' + pay;
        count.innerHTML = ' ' + '*' + item3;

    }else{
        var count = document.getElementById('greenc');
        count.innerHTML = ' ';
    }
}
function setChai(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var pay = data.product_price * data.product_quantity;
        item4 = data.product_quantity;
        var subtotal = document.getElementById('chaipay');
        var count = document.getElementById('chaic');
        subtotal.innerHTML = 'NT ' + pay;
        count.innerHTML = ' ' + '*' + item4;

        
    }else{
        var count = document.getElementById('chaic');
        count.innerHTML = ' ';
    }
}
function setMint(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var pay = data.product_price * data.product_quantity;
        item5 = data.product_quantity;
        var subtotal = document.getElementById('mintpay');
        var count = document.getElementById('mintc');
        subtotal.innerHTML = 'NT ' + pay;
        count.innerHTML = ' ' + '*' + item5;
    }else{
        var count = document.getElementById('mintc');
        count.innerHTML = ' ';
    }
}
function setFruit(snapshot){
    if(snapshot.exists()){
        var data = snapshot.val();
        var pay = data.product_price * data.product_quantity;
        item6 = data.product_quantity;
        var subtotal = document.getElementById('fruitpay');
        var count = document.getElementById('fruitc');
        subtotal.innerHTML = 'NT ' + pay;
        count.innerHTML = ' ' + '*' + item6;
    }else{
        var count = document.getElementById('fruitc');
        count.innerHTML = ' ';
    }
}
