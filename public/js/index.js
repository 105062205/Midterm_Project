var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

function init(){
   
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('userbtn');
        var shopnow = document.getElementById('shopbtn');
        if (user) {
            var userId = user.uid;
            console.log(userId);
            user_email = user.email;
            menu.innerHTML = "<a class='btn btn-light btn-xl js-scroll-trigger' id='logout-btn'>LogOut</a>"
            shopnow.innerHTML ="<a class='btn btn-light btn-xl sr-button' href='cart.html'>Shop Now!</a>"
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            var orderRef = firebase.database().ref('users').child(userId + '/order');
            orderRef.once('value', getData);

        } else {
            menu.innerHTML = "<a class='btn btn-light btn-xl js-scroll-trigger' href='auth.html'>Sign Up/Log In</a>";
            shopnow.innerHTML ="<a class='btn btn-light btn-xl sr-button' href='auth.html'>LogIn First!</a>"
            //document.getElementById('post_list').innerHTML = "";
            clear();
        }
    });
    

}
function clear(){
    var btotal = document.getElementById('breakpay');
    var bcount = document.getElementById('breakc');
    btotal.innerHTML = 'NT ' + 0;
    bcount.innerHTML = ' ';

    var etotal = document.getElementById('earlpay');
    var ecount = document.getElementById('earlc');
    etotal.innerHTML = 'NT ' + 0;
    ecount.innerHTML = ' ';

    var gtotal = document.getElementById('greenpay');
    var gcount = document.getElementById('greenc');
    gtotal.innerHTML = 'NT ' + 0;
    gcount.innerHTML = ' ';

    var ctotal = document.getElementById('chaipay');
    var ccount = document.getElementById('chaic');
    ctotal.innerHTML = 'NT ' + 0;
    ccount.innerHTML = ' ';
    
    var mtotal = document.getElementById('mintpay');
    var mcount = document.getElementById('mintc');
    mtotal.innerHTML = 'NT ' + 0;
    mcount.innerHTML = ' ';

    var ftotal = document.getElementById('fruitpay');
    var fcount = document.getElementById('fruitc');
    ftotal.innerHTML = 'NT ' + 0;
    fcount.innerHTML = ' ';

    var total = document.getElementById('totalpay');
    total.innerHTML = 'NT ' + 0;
}
function getData(snapshot){
    var data = snapshot.val();
    console.log(data.breakfast);

    var btotal = document.getElementById('breakpay');
    var bcount = document.getElementById('breakc');
    btotal.innerHTML = 'NT ' + data.breakfast*185;
    bcount.innerHTML = ' ' + '*' + data.breakfast;

    var etotal = document.getElementById('earlpay');
    var ecount = document.getElementById('earlc');
    etotal.innerHTML = 'NT ' + data.earlgrey*185;
    ecount.innerHTML = ' ' + '*' + data.earlgrey;

    var gtotal = document.getElementById('greenpay');
    var gcount = document.getElementById('greenc');
    gtotal.innerHTML = 'NT ' + data.green*225;
    gcount.innerHTML = ' ' + '*' + data.green;

    var ctotal = document.getElementById('chaipay');
    var ccount = document.getElementById('chaic');
    ctotal.innerHTML = 'NT ' + data.chai*220;
    ccount.innerHTML = ' ' + '*' + data.chai;
    
    var mtotal = document.getElementById('mintpay');
    var mcount = document.getElementById('mintc');
    mtotal.innerHTML = 'NT ' + data.mint*250;
    mcount.innerHTML = ' ' + '*' + data.mint;

    var ftotal = document.getElementById('fruitpay');
    var fcount = document.getElementById('fruitc');
    ftotal.innerHTML = 'NT ' + data.fruit*300;
    fcount.innerHTML = ' ' + '*' + data.fruit;

    var total = document.getElementById('totalpay');
    total.innerHTML = 'NT ' + data.payment;

}
function on(){
    modal.style.display = "block";
}
span.onclick = function() {
    modal.style.display = "none";
}
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}